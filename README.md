# Kubernetes Workshop
Lab 02: Creating and scaling your first pod

---

## Instructions


### Create a Namespace

 - Check the current namespaces
```
kubectl get namespaces
```

 - Let's create a namespace by creating a file called "namespace.yaml" with the content below:
```
apiVersion: v1
kind: Namespace
metadata:
  name: dev
```

 - Create the namespace using kubectl
```
kubectl create -f namespace.yaml
```

 - Ensure that the namespace was created successfully
```
kubectl get namespaces
```

 - set the created namespace as a default namespace
```
kubectl config set-context --current --namespace=dev
```

### Creating Our First Pod

 - Inspect the pod definition
```
curl https://gitlab.com/sela-aks-workshop/lab-02/raw/master/echoserver-pod.yaml
```

 - List existent pods (before create our first pod)
```
kubectl get pods
```

 - Create the pod
```
kubectl create -f https://gitlab.com/sela-aks-workshop/lab-02/raw/master/echoserver-pod.yaml
```

 - List existent pods (to ensure our pod was created)
```
kubectl get pods
```

### Creating Our First ReplicaSet

 - Inspect the ReplicaSet definition
```
curl https://gitlab.com/sela-aks-workshop/lab-02/raw/master/echoserver-rs.yaml
```

 - List existent ReplicaSets
```
kubectl get replicasets
```

 - Create the ReplicaSet
```
kubectl create -f https://gitlab.com/sela-aks-workshop/lab-02/raw/master/echoserver-rs.yaml
```

 - List existent ReplicaSets
```
kubectl get rs
```

 - List existent pods
```
kubectl get pods
```

### Scaling our application using ReplicaSets

 - Inspect the ReplicaSet
```
kubectl describe rs echoserver
```

 - Edit the ReplicaSet to update "replicas" from "3" to "5"
```
kubectl edit rs echoserver
```

 - List existent ReplicaSets
```
kubectl get rs
```

 - List existent pods 
```
kubectl get pods
```

### Understanding the difference between Pods and ReplicaSets

 - In the previous command you should see 6 pods (1 from the first pod and 5 from the replicaset)
```
NAME               READY   STATUS    RESTARTS   AGE
echoserver         1/1     Running   0          5m36s
echoserver-gqp8w   1/1     Running   0          26s
echoserver-h8xs5   1/1     Running   0          3m2s
echoserver-pktkp   1/1     Running   0          26s
echoserver-vc5fc   1/1     Running   0          3m2s
```

 - To understand the difference let's delete all the pods in the namespace by run:
```
kubectl delete pod --all
```

 - Then list all pods to see what happen
```
kubectl get pods
```

 - Note that the pods managed by the replicaset were recreated but the single pod was deleted
```
NAME               READY   STATUS    RESTARTS   AGE
echoserver-7sckg   1/1     Running   0          96s
echoserver-m8rgj   1/1     Running   0          96s
echoserver-q7455   1/1     Running   0          96s
echoserver-vkglp   1/1     Running   0          96s
echoserver-zpp5l   1/1     Running   0          96s
```

### Cleanup

 - List existent resources in your namespace
```
kubectl get all
```

 - Delete existent resources
```
kubectl delete all --all
```

 - List existent resources to ensure that the environment is clean
```
kubectl get all
```
